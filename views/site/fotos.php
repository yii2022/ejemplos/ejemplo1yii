<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
?>

<?php
    //utilizando el helper img
    echo Html::img("@web/imgs/{$foto}", [
    'id' => 'foto',
    'alt' => 'foto leon',
    'width' => '300'
    ]) 
?>

<?php//utilizando el helper URL?>
<img src="<?= Url::to("@web/imgs/{$foto}")?>" width="300">

<?php//utilizando el metodo getAlias?>
<img src="<?= Yii::getAlias('@web') . "/imgs/{$foto}"?>" width="300">